# Dnevnik rada

### 2018-01-23

Dovrsetak izrada dnevnika rada montera za projekt. Prikaz unosa. Modal za uredjivanje i dodavanje. Dorada baze. Skripte za spremanje u bazu. Dodavanje i uredjivanje.

![status stavke](https://edinlockedin.bitbucket.io/images/status_stavke-modal.png)

![radni sati](https://edinlockedin.bitbucket.io/images/radni_sati.png)

### 2018-01-22

Izrada dnevnika rada montera za projekt. Prikaz unosa. Modal za uredjivanje i dodavanje. Dorada baze.

### 2018-01-19

Nastavak rada na izvodjenju. Upload na server i dorada. Pocetak rada na statusu stavke za situaciju.

### 2018-01-18

Izrada procedure za dodavanje logova za stavku. Razni izracuni radi statistike i uvjetni unosi.

### 2018-01-17

Prethodna ideja odbacena. Krivo shvatio, ne poklapa se sa potrebama.

Dorada modala i podataka koje prihvaca. Zapisuju se podaci samo za tu stavku, bez specificiranja vremena putovanja i montera koji su radili na stavci. Ti podaci se izmjestaju u radne sate.

![status stavke](https://edinlockedin.bitbucket.io/images/status_stavke-modal.png)

### 2018-01-16

Dorada tablica za izvodjenje i za situaciju. Dodana skripte za dohvacanje izvodjenja. Skica prikaza stavki i modala za unos izvodjenja. Pocetno dohvacanje za NTR.

![tablica](https://edinlockedin.bitbucket.io/images/tablica.png)

![modal](https://edinlockedin.bitbucket.io/images/modal1.png)![modal_povijest](https://edinlockedin.bitbucket.io/images/modal2.png)


### 2018-01-15

Popravak izracuna projektne cijene sata. Popravak izracuna u modalu s izvjestajem. Dodan izracun za putne troskove.

Razrada taba za izvodjenje. Skiciranje baze.  

### 2018-01-12

Ispravak krivog izracuna kod promijene marzi u parametrima. Nakon spremanja izmjena marzi u bazu za stavke treba ponovno napraviti izracun za sve stavke potom napraviti ostatak izracuna.

```SQL
DELIMITER $$
CREATE PROCEDURE `recalculateItems`(IN `tenderId` int(10))
BEGIN
    UPDATE items
    JOIN tender ON tender.id = items.tender_id
    SET
        vss_sum := items.vss * items.item_unit_quantity,
        sss_sum := items.sss * items.item_unit_quantity,
        rss_sum := items.rss * items.item_unit_quantity,
        material_unit := items.unit_price - (items.unit_price * IF( items.rebate=0, 0, items.rebate/100 )),
        material_sum := (items.unit_price - (items.unit_price * IF( items.rebate=0, 0, items.rebate/100 ))) * items.item_unit_quantity,
        personnel_unit := (items.vss * tender.engineer_cost) + (items.sss * tender.assembler_cost) + (items.rss * tender.utils_cost),
        personnel_sum := ((items.vss * tender.engineer_cost) + (items.sss * tender.assembler_cost) + (items.rss * tender.utils_cost)) * items.item_unit_quantity,
        material_personnel_unit := (items.unit_price - (items.unit_price * IF( items.rebate=0, 0, items.rebate/100 )))
            + ((items.vss * tender.engineer_cost) + (items.sss * tender.assembler_cost) + (items.rss * tender.utils_cost)),
        material_personnel_sum := ((items.unit_price - (items.unit_price * IF( items.rebate=0, 0, items.rebate/100 )))
            + ((items.vss * tender.engineer_cost) + (items.sss * tender.assembler_cost) + (items.rss * tender.utils_cost))) * items.item_unit_quantity,
        earnings_material := ((items.unit_price - (items.unit_price * IF( items.rebate=0, 0, items.rebate/100 ))) * items.item_unit_quantity) * IF(items.item_margin_material=0, 0, items.item_margin_material/100),
        earnings_personnel := (((items.vss * tender.engineer_cost) + (items.sss * tender.assembler_cost) + (items.rss * tender.utils_cost)) * items.item_unit_quantity) * IF(items.item_margin_personnel=0, 0, items.item_margin_personnel/100),
        item_vss_earning := (items.vss * items.item_unit_quantity) * tender.engineer_cost * IF(items.item_margin_personnel=0, 0, items.item_margin_personnel/100),
        item_sss_earning := (items.sss * items.item_unit_quantity) * tender.assembler_cost * IF(items.item_margin_personnel=0, 0, items.item_margin_personnel/100),
        item_rss_earning := (items.rss * items.item_unit_quantity) * tender.utils_cost * IF(items.item_margin_personnel=0, 0, items.item_margin_personnel/100),
        earnings_sum := (((items.unit_price - (items.unit_price * IF( items.rebate=0, 0, items.rebate/100 ))) * items.item_unit_quantity) * IF(items.item_margin_material=0, 0, items.item_margin_material/100))
            + ((((items.vss * tender.engineer_cost) + (items.sss * tender.assembler_cost) + (items.rss * tender.utils_cost)) * items.item_unit_quantity) * IF(items.item_margin_personnel=0, 0, items.item_margin_personnel/100))
    WHERE tender_id = tenderId;

END $$
DELIMITER ;
```

### 2018-01-11

Implementacija novih izracuna u sucelje. Umjesto azuriranja finalnih cijena jedne stavke sada se moraju azurirati za sve stavke. U istom zahtjevu dohvacamo podatke o ponudi, parametre, izracun cijele ponude i izracun za sve stavke. Pri azuriranju ili stavke ili parametra sve iznosi se moraju nanovo iscrtati. Vjerojatno smo izgubili na performansama unatoc manjem broju zahtjeva prema serveru. Vidjet cemo na vecim troskovnicima.

```SQL
DROP PROCEDURE IF EXISTS `getItemsCalcs`;;
CREATE PROCEDURE `getItemsCalcs`(IN `tenderId` int(10))
BEGIN

    SELECT
        item_id,
        unit_price,
        unit_price_set,
        rebate,
        item_margin_material,
        item_margin_personnel,
        vss,
        sss,
        rss,
        vss_sum,
        sss_sum,
        rss_sum,
        material_unit,
        material_sum,
        personnel_unit,
        personnel_sum,
        material_personnel_unit,
        material_personnel_sum,
        earnings_material,
        earnings_personnel,
        item_vss_earning,
        item_sss_earning,
        item_rss_earning,
        earnings_sum,
        earnings_material_percentage,
        earnings_personnel_percentage,
        material_share,
        personnel_share,
        item_material_cost,
        item_personnel_cost,
        final_tender_unit,
        final_tender_sum
    FROM items
    WHERE items.tender_id = tenderId;

END;;

DROP PROCEDURE IF EXISTS `getTenderCalcs`;;
CREATE PROCEDURE `getTenderCalcs`(IN `tenderId` int(10))
BEGIN

    -- 1. Dnevnice i cijena kilometra
    SET @cr := (SELECT currency.rate FROM currency
        INNER JOIN country ON country.currency = currency.sign
        INNER JOIN tender ON tender.country = country.id
        WHERE tender.id = tenderId);

    SELECT country.km_cost * @cr, country.daily_allowance * @cr INTO @km, @diem FROM tender
        INNER JOIN country ON country.id = tender.country
        WHERE tender.id = tenderId;

    -- 2. Sume svih vrijednosti stavki
    SELECT
        -- nabavna cijena robe
        sum(items.material_sum),
        -- sveukupno rad
        sum(items.personnel_sum),
        -- sati rada
        sum(items.vss_sum),
        sum(items.sss_sum),
        -- trosak rada inzinjera
        sum(items.vss_sum) * tender.engineer_cost,
        -- trosak rada montera
        sum(items.sss_sum) * tender.assembler_cost,
        -- suma vrijednosti stavki (H:H)
        sum(items.final_tender_sum),
        -- marza na materijalu
        sum(items.earnings_material),
        -- marza na radu
        sum(items.earnings_personnel),
        -- marze po tipu zaposlenika
        sum(items.item_vss_earning),
        sum(items.item_sss_earning)
    INTO
        @sum_material,
        @sum_personnel,
        @sum_vss_sum,
        @sum_sss_sum,
        @total_engineer_cost,
        @total_assembler_cost,
        @sum_final_tender_sum,
        @sum_earnings_material,
        @sum_earnings_personnel,
        @item_vss_earning,
        @item_sss_earning
    FROM items
    INNER JOIN tender ON tender.id = items.tender_id
    WHERE tender.id = tenderId;

    -- 2. Sume svih vrijednosti stavki
    SELECT
        -- nabavna cijena robe
        FORMAT(@sum_material, 2, 'hr_HR') AS sum_material,
        -- sveukupno rad
        FORMAT(@sum_personnel, 2, 'hr_HR') AS sum_personnel,
        -- sati rada
        FORMAT(@sum_vss_sum, 2, 'hr_HR') AS sum_vss_sum,
        FORMAT(@sum_sss_sum, 2, 'hr_HR') AS sum_sss_sum,
        -- trosak rada inzinjera
        FORMAT(@total_engineer_cost, 2, 'hr_HR') AS total_engineer_cost,
        -- trosak rada montera
        FORMAT(@total_assembler_cost, 2, 'hr_HR') AS total_assembler_cost,
        -- cestarina
        @total_toll := tender.toll * tender.field_commutes,
        FORMAT(@total_toll, 2, 'hr_HR') AS total_toll,
        -- kilometraza na putu
        @total_km_cost := (@km * tender.returning_distance) * tender.field_commutes,
        FORMAT(@total_km_cost, 2, 'hr_HR') AS total_km_cost,
        -- dnevnice
        @total_diem := @diem * tender.diem_num_per_employee * tender.field_employees,
        FORMAT(@total_diem, 2, 'hr_HR') AS total_diem,
        -- nocenja
        @total_overnight := tender.field_employees * tender.overnight_stays * tender.overnight_cost,
        FORMAT(@total_overnight, 2, 'hr_HR') AS total_overnight,
        -- sveukupni troskovi gradilista
        @total_construction_cost := tender.misc_cost + tender.third_party_transport + tender.insurence_cost + tender.additional_equipment_cost,
        FORMAT(@total_construction_cost, 2, 'hr_HR') AS total_construction_cost,
        -- kilometraza dostava
        @total_delivery := tender.delivery_distance * @km,
        FORMAT(@total_delivery, 2, 'hr_HR') AS total_delivery,
        -- garancije
        @total_guarantees := tender.bank_guarantee_bid + tender.bank_guarantee_exec,
        FORMAT(@total_guarantees, 2, 'hr_HR') AS total_guarantees,
        FORMAT(tender.project_management_hours, 2, 'hr_HR') AS project_management_hours,
        @total_project_managament_cost := tender.project_management_hours * tender.engineer_cost,
        FORMAT(@total_project_managament_cost, 2, 'hr_HR') AS total_project_managament_cost,
        -- sveukpni troskovi robe
        @material_cost := @total_delivery + @total_construction_cost + @total_guarantees + @total_project_managament_cost,
        FORMAT(@material_cost, 2, 'hr_HR') AS material_cost,
        -- sveukpni troskovi rada
        @personnel_cost := @total_toll + @total_km_cost + @total_diem + @total_overnight,
        FORMAT(@personnel_cost, 2, 'hr_HR') AS personnel_cost,
        -- zavisni troskovi projekta
        @dependent_cost := @material_cost + @personnel_cost,
        FORMAT(@dependent_cost, 2, 'hr_HR') AS dependent_cost,
        -- sveukupni troskovi projekta s nabavnom cijenom robe
        @total_project_before_margin := (@sum_material + @sum_personnel + @dependent_cost),
        FORMAT(@total_project_before_margin, 2, 'hr_HR') AS total_project_before_margin,
        -- suma vrijednosti stavki (H:H)
        FORMAT(@sum_final_tender_sum, 2, 'hr_HR') AS sum_final_tender_sum,
        -- ukupna planirana prodajna cijena
        @total_tender_final_price := @sum_final_tender_sum - ( @sum_final_tender_sum * IF( tender.sum_discount=0, 0, tender.sum_discount/100 ) ),
        FORMAT(@total_tender_final_price, 2, 'hr_HR') AS total_tender_final_price,
        -- ukupna planirana zarada
        @total_earnings := @total_tender_final_price - @total_project_before_margin,
        FORMAT(@total_earnings, 2, 'hr_HR') AS total_earnings,
        -- marza na materijalu
        FORMAT(@sum_earnings_material, 2, 'hr_HR') AS sum_earnings_material,
        -- marza na radu
        FORMAT(@sum_earnings_personnel, 2, 'hr_HR') AS sum_earnings_personnel,
        -- marze po tipu zaposlenika
        FORMAT(@item_vss_earning, 2, 'hr_HR') AS item_vss_earning,
        FORMAT(@item_sss_earning, 2, 'hr_HR') AS item_sss_earning,
        -- sveukupna planirana marza
        FORMAT(@sum_earnings_material + @item_vss_earning + @item_sss_earning, 2, 'hr_HR') AS total_planned_margin,
        -- projektna_cijena sata
        FORMAT((@sum_personnel + @personnel_cost) / IF((@sum_vss_sum + @sum_sss_sum) = 0, 1, (@sum_vss_sum + @sum_sss_sum)), 2, 'hr_HR') AS hourly_rate,
        -- udjeli
        -- nabavna cijena robe
        FORMAT(IF( @total_project_before_margin = 0, 0, (@sum_material / @total_project_before_margin) * 100 ), 2, 'hr_HR') AS sum_material_percentage,
        -- trosak rada inzinjera
        FORMAT(IF( @total_project_before_margin = 0, 0, (@total_engineer_cost / @total_project_before_margin) * 100 ), 2, 'hr_HR') AS total_engineer_cost_percentage,
        -- trosak rada montera
        FORMAT(IF( @total_project_before_margin = 0, 0, (@total_assembler_cost / @total_project_before_margin) * 100 ), 2, 'hr_HR') AS total_assembler_cost_percentage,
        -- dnevnice
        FORMAT(IF( @total_project_before_margin = 0, 0, (@total_diem / @total_project_before_margin) * 100 ), 2, 'hr_HR') AS total_diem_percentage,
        -- nocenja
        FORMAT(IF( @total_project_before_margin = 0, 0, (@total_overnight / @total_project_before_margin) * 100 ), 2, 'hr_HR') AS total_overnight_percentage,
        -- vodjenje projekta
        FORMAT(IF( @total_project_before_margin = 0, 0, (@total_project_managament_cost / @total_project_before_margin) * 100 ), 2, 'hr_HR') AS total_project_managament_cost_percentage,
        -- ukupno troskovi
        FORMAT(IF( @total_project_before_margin = 0, 0, (@dependent_cost / @total_project_before_margin) * 100 ), 2, 'hr_HR') AS dependent_cost_percentage,
        -- marza rad
        FORMAT(IF( @total_project_before_margin = 0, 0, (@sum_earnings_personnel / @total_project_before_margin) * 100 ), 2, 'hr_HR') AS sum_earnings_personnel_percentage,
        -- marza roba
        FORMAT(IF( @total_project_before_margin = 0, 0, (@sum_earnings_material / @total_project_before_margin) * 100 ), 2, 'hr_HR') AS sum_earnings_material_percentage,
        -- sume
        -- nepredviđeni radovi na iznos
        @uwr := IF(tender.unforeseen_work=0, 0, tender.unforeseen_work/100),
        @uwt := @total_tender_final_price * @uwr,
        FORMAT(@uwt, 2, 'hr_HR') AS uwt,
        -- ukupan iznos
        @total_uw := @total_tender_final_price + (@total_tender_final_price * @uwr),
        FORMAT(@total_uw, 2, 'hr_HR') AS total_uw
    FROM tender
    WHERE tender.id = tenderId;

END;;

DROP PROCEDURE IF EXISTS `getTenderWithCalcs`;;
CREATE PROCEDURE `getTenderWithCalcs`(IN `tenderId` int(10) unsigned)
BEGIN

SELECT
        tender.id,
        tender.project_id,
        tender.generated_label,
        tender.name as name,
        tender.description,
        tender.client as client,
        client.name as client_name,
        FORMAT(tender.sum_discount, 2, 'hr_HR') AS sum_discount,
        tender.country,
        tender.include_vat,
        tender.tender_accepted,
        tender.tender_accepted_on_date,
        tender.lead as lead,
        user.name as lead_name,
        tender.payment_method,
        tender.deadline,
        tender.warranty_period,
        tender.contract,
        FORMAT(tender.contract_value, 2, 'hr_HR') AS contract_value,
        tender.remarks,
        tender.introductory,
        tender.signature_supervisor,
        tender.signature_client,
        tender.signature_contractor,
        FORMAT(engineer_cost, 2, 'hr_HR') AS engineer_cost,
        FORMAT(assembler_cost, 2, 'hr_HR') AS assembler_cost,
        FORMAT(utils_cost, 2, 'hr_HR') AS utils_cost,
        FORMAT(km_cost, 2, 'hr_HR') AS km_cost,
        FORMAT(diem, 2, 'hr_HR') AS diem,
        FORMAT(diem_num_per_employee, 2, 'hr_HR') AS diem_num_per_employee,
        FORMAT(field_employees, 2, 'hr_HR') AS field_employees,
        FORMAT(overnight_cost, 2, 'hr_HR') AS overnight_cost,
        FORMAT(overnight_stays, 2, 'hr_HR') AS overnight_stays,
        FORMAT(returning_distance, 3, 'hr_HR') AS returning_distance,
        FORMAT(toll, 2, 'hr_HR') AS toll,
        FORMAT(field_commutes, 2, 'hr_HR') AS field_commutes,
        FORMAT(delivery_distance, 3, 'hr_HR') AS delivery_distance,
        FORMAT(misc_cost, 2, 'hr_HR') AS misc_cost,
        FORMAT(third_party_transport, 2, 'hr_HR') AS third_party_transport,
        FORMAT(insurence_cost, 2, 'hr_HR') AS insurence_cost,
        FORMAT(additional_equipment_cost, 2, 'hr_HR') AS additional_equipment_cost,
        FORMAT(bank_guarantee_bid, 2, 'hr_HR') AS bank_guarantee_bid,
        FORMAT(bank_guarantee_exec, 2, 'hr_HR') AS bank_guarantee_exec,
        FORMAT(margin_material, 2, 'hr_HR') AS margin_material,
        FORMAT(margin_personnel, 2, 'hr_HR') AS margin_personnel,
        FORMAT(unforeseen_work, 2, 'hr_HR') AS unforeseen_work,
        FORMAT(project_management_hours, 2, 'hr_HR') AS project_management_hours
    FROM tender
    LEFT JOIN client ON client.id = tender.client
    LEFT JOIN user ON user.id = tender.lead
    WHERE tender.id = tenderId LIMIT 1;

    CALL updateCalculations(tenderId);
    CALL getTenderCalcs(tenderId);
    CALL getItemsCalcs(tenderId);

END;;

DROP PROCEDURE IF EXISTS `updateCalculations`;;
CREATE PROCEDURE `updateCalculations`(IN `tenderId` int(10))
BEGIN

    -- 1. Dnevnice i cijena kilometra
    SET @cr := (SELECT currency.rate FROM currency
        INNER JOIN country ON country.currency = currency.sign
        INNER JOIN tender ON tender.country = country.id
        WHERE tender.id = tenderId);

    SELECT country.km_cost * @cr, country.daily_allowance * @cr INTO @km, @diem FROM tender
    INNER JOIN country ON country.id = tender.country
    WHERE tender.id = tenderId;

    -- 2. Sume svih vrijednosti stavki
    SELECT
        sum(material_sum),
        sum(personnel_sum),
        sum(earnings_material),
        sum(earnings_personnel),
        (delivery_distance * @km) + misc_cost + third_party_transport + insurence_cost + additional_equipment_cost + bank_guarantee_bid + bank_guarantee_exec + (project_management_hours * engineer_cost),
        (toll * field_commutes) + ((@km * returning_distance) * field_commutes) + (@diem * diem_num_per_employee * field_employees) + (field_employees * overnight_stays * overnight_cost)
    INTO
        @sum_material,
        @sum_personnel,
        @sum_earnings_material,
        @sum_earnings_personnel,
        @material_cost,
        @personnel_cost
    FROM tender
    INNER JOIN items ON items.tender_id = tender.id
    WHERE tender.id = tenderId;
    -- 3. Update stavki
    UPDATE items SET
        material_share := material_sum / if(@sum_material=0,1,@sum_material),
        personnel_share := personnel_sum / if(@sum_personnel=0,1,@sum_personnel),
        item_material_cost := (material_sum / if(@sum_material=0,1,@sum_material)) * @material_cost,
        item_personnel_cost := (personnel_sum / if(@sum_personnel=0,1,@sum_personnel)) * @personnel_cost,
        final_tender_sum := items.material_sum + items.personnel_sum + items.earnings_sum + ((items.material_sum / if(@sum_material=0,1,@sum_material)) * @material_cost) + ((items.personnel_sum / if(@sum_personnel=0,1,@sum_personnel)) * @personnel_cost),
        final_tender_unit := (items.material_sum + items.personnel_sum + items.earnings_sum + ((items.material_sum / if(@sum_material=0,1,@sum_material)) * @material_cost) + ((items.personnel_sum / if(@sum_personnel=0,1,@sum_personnel)) * @personnel_cost)) / items.item_unit_quantity
    WHERE tender_id = tenderId;

END;;

DROP PROCEDURE IF EXISTS `updateItem`;;
CREATE PROCEDURE `updateItem`(IN `colName` varchar(40), IN `colValue` decimal(12,4), IN `itemId` int(10))
BEGIN

    SET @tid := (SELECT tender_id FROM items WHERE item_id = itemId LIMIT 1);

    -- 1. Update vrijednosti stavke
    SET @col := colName;
    SET @sql := CONCAT("UPDATE items SET ", @col, " = ? WHERE item_id = ?;");
    PREPARE stmt FROM @sql;
    SET @val := colValue;
    SET @iid := itemId;
    EXECUTE stmt USING @val, @iid;
    DEALLOCATE PREPARE stmt;

    -- 2. Izracun stavke sa marzom
    SELECT
        @calc_rebate := IF( i.rebate=0, 0, i.rebate/100 ) AS rebate,
        @margin_material := IF(i.item_margin_material=0, 0, i.item_margin_material/100) AS margin_material,
        @margin_personnel := IF(i.item_margin_personnel=0, 0, i.item_margin_personnel/100) AS margin_personnel,
        @vss_sum := i.vss * i.item_unit_quantity AS vss_sum,
        @sss_sum := i.sss * i.item_unit_quantity AS sss_sum,
        @rss_sum := i.rss * i.item_unit_quantity AS rss_sum,
        @material_unit := i.unit_price - (i.unit_price * @calc_rebate) AS material_unit,
        @material_sum := @material_unit * i.item_unit_quantity AS material_sum,
        @personnel_unit := (i.vss * t.engineer_cost) + (i.sss * t.assembler_cost) + (i.rss * t.utils_cost) AS personnel_unit,
        @personnel_sum := @personnel_unit * i.item_unit_quantity AS personnel_sum,
        @material_personnel_unit := @material_unit + @personnel_unit AS material_personnel_unit,
        @material_personnel_sum := @material_personnel_unit * i.item_unit_quantity AS material_personnel_sum,
        @earnings_material := @material_sum * @margin_material AS earnings_material,
        @earnings_personnel := @personnel_sum * @margin_personnel AS earnings_personnel,
        @item_vss_earning := @vss_sum * t.engineer_cost * @margin_personnel AS item_vss_earning,
        @item_sss_earning := @sss_sum * t.assembler_cost * @margin_personnel AS item_sss_earning,
        @item_rss_earning := @rss_sum * t.utils_cost * @margin_personnel AS item_rss_earning,
        @earnings_sum := @earnings_material + @earnings_personnel AS earnings_sum
    FROM items i
    INNER JOIN tender t ON t.id = i.tender_id
    WHERE item_id = itemId LIMIT 1;

    UPDATE items SET
        vss_sum := @vss_sum,
        sss_sum := @sss_sum,
        rss_sum := @rss_sum,
        material_unit := @material_unit,
        material_sum := @material_sum,
        personnel_unit := @personnel_unit,
        personnel_sum := @personnel_sum,
        material_personnel_unit := @material_personnel_unit,
        material_personnel_sum := @material_personnel_sum,
        earnings_material := @earnings_material,
        earnings_personnel := @earnings_personnel,
        item_vss_earning := @item_vss_earning,
        item_sss_earning := @item_sss_earning,
        item_rss_earning := @item_rss_earning,
        earnings_sum := @earnings_sum
    WHERE item_id = itemId;

    CALL updateCalculations(@tid);
    CALL getTenderCalcs(@tid);
    CALL getItemsCalcs(@tid);

END;;

DROP PROCEDURE IF EXISTS `updateParameter`;;
CREATE PROCEDURE `updateParameter`(IN `colName` varchar(40), IN `colValue` decimal(12,4), IN `tenderId` int(10))
BEGIN

    -- 1. Update vrijednosti parametra
    SET @col := colName;
    SET @sql := CONCAT("UPDATE tender SET ", @col, " = ? WHERE id = ?;");
    PREPARE stmt FROM @sql;
    SET @val := colValue;
    SET @tid := tenderId;
    EXECUTE stmt USING @val, @tid;
    DEALLOCATE PREPARE stmt;

    CALL updateCalculations(@tid);
    CALL getTenderCalcs(@tid);
    CALL getItemsCalcs(@tid);

END;;

```

### 2018-01-10

Izrada procedura korak po korak prema Jeleninim formulama u Excel kalkulacijskom. Treba napraviti pocetne izracune u istom koraku s azuriranjem vrijednosti zatim izracune s dohvacanjem parametara i na kraju napraviti izracun za cijeli projekt

### 2018-01-09

Izrada nove logike za izracune s obzirom da je prva verzija bila neispravna. Stavljanje koraka na papir i pocetna izrada procedura.

### 2018-01-08

TODO
- ~~napraviti promijene u parametrima s obzirom na promijene sa dnevnicama~~
- ~~procedura za izracun prema parametrima po uzoru na izracun stavki~~

### 2018-01-05

TODO
- (__prebaceno__) napraviti promijene u parametrima s obzirom na promijene sa dnevnicama
- (__prebaceno__) procedura za izracun prema parametrima
- ~~procedura za izracun stavki~~
    - procedura radi update izmijenjene vrijednosti zatim select svih izracuna potom update izracuna
    - izracun se vraca aplikaciji i prikazuje ih za tu stavku
    - radi i ponovno dohvacanje izracuna za projekt, ali to je sada samo privremeno

Razni popravci poput decimala, dodatnih izracuna...

### 2018-01-04

TODO
- ~~samo jedna ponuda unutar projekta moze biti prihvacena~~
- ~~ispraviti datum prihvacanja ponude~~
- ~~ponovno iscrtati dash nakon spremanja izmjena na ponudi~~
- ~~popraviti 'narucitelj nije investitor'~~

Sve je postavljeno na server.

### 2018-01-03

Zavrseni osnovni podaci za ponudu sa spremanjem u bazu. Neka polja su ogranicena na odredjene unose poput klijenata, korisnika...

Dodana tablica sa zemljama. Iznos dnevnica i cijena kilometra su sada vezane za njih. Promijene na tablici ponuda. Izbacena valuta kao constraint. Ona je sada vezana za zemlju koja je postala constraint. Dodan inicijalni JavaScript Country objekt sa par metoda.

Mala izmjena Markove skripte za dohvacanje tecaja sa HNB-a. Plan je da se izvrsava jednom dnevno i upisuje nove tecaje u bazu.

### 2018-01-02

Zavrsena je forma za osnovne ponude. Treba dodati jos par filter lista za laksi unos. Spremne su metode za prikupljanje i spremanje podataka. Dodana je nova procedura za unos u bazu. Forma je povezana s odabirom ponude u listi.

Dodana je tablica sa zemljama i povezana sa valutama. Sadrzi osnovne podatke i iznose dnevnica i cijene kilometra po drzavi. Treba provjeriti sluzbene podatke.

### 2017-12-29

Zavrseno dodavanje, uredjivanje i spremanje podataka o djelatnicima i klijentima. Prilagodba prikaza prema ovlastima prijavljenog korisnika.

Nastavak uredjivanja taba s osnovnim podacima o ponudi. Izmjena procedure za dohvacenje podataka koji mi trebaju za prikaz.

### 2017-12-28

Dodana SQL procedura za `insert` ponude. Dorada procedure za kopiranje ponude. Sitni popravci na procedurama i backend skriptama.

Dodan tab za osnovne podatke o ponudi. Cekam feedback za nastavak rada na spremanju.

![stavke](https://edinlockedin.bitbucket.io/images/tender_inputs.png)

Dorade na korisnicima. Neke stavke (kod, ulogu u firmi, (ne)aktivan) mogu mijenjati samo ovlasteni. Ostali mogu mijenjati samo dijelove vlastitog profila (ime, email, lozinka, telefon).

### 2017-12-27

Napravio sam promijene kod izbornika. Poveznice vise nisu razdijeljene izmedju projekta i ponude vec se sve nalaze pod projektima.

~~Prvo sam mislio odvojiti `NTR` i `Dnevnik rada` da oni koji upisuju ne moraju prelaziti s jedne stranice (popis projekata), da bi odabrali projekt, potom na drugu da bi upisali sto su mislili upisati nego bi to radili iz padajuceg izbornika na istoj stranici. Markova ideja je da je popis projekata svima vidljiv i da je on osnovno sredstvo za odabir projekata i ponuda. Oni koji nemaju ovlasti za raditi direktne promijene na projektima/ponudama vidjeli bi samo projekte koji su u izvedbi.~~

Promijene kod odabira projekta odnosno ponude iz popisa. Prvi redak oznacava razinu ovlasti na projektu, prvi stupac akciju (sto se odabralo).

|             | __lvl1__                               | __lvl2__   | __lvl3__ |
| ----------  | -------------------------------------- | ---------- | -------- |
| __projekt__ | ovlasti, ntr                           | ntr        | ntr      |
| __ponuda__  | parametri, troskovnik, izvodjenje, vtr | izvodjenje | -        |


Napravio sam osnovno cachiranje/spremanje u predmemoriju podataka o projektima i ponudama (parametri, troskovnik, ovlasti...) radi performansi i smanjenja opterecenja na server.

SQL procedure za `update` projekta i ponude.

Sitne promijene u organizaciji koda.
